using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.


public class Fruit : MonoBehaviour
{
    [Header("Board Variables")]
    public int column;
    public int row;
    public int previousColumn;
    public int previousRow;
    public int targetX;
    public int targetY;
    public bool isMatched = false;

    private FindMatches findMatches;
    private BoardManager board;
    public GameObject otherFruit;
    private Vector2 firstTouchPos;
    private Vector2 finalTouchPos;
    private Vector2 tempTouchPos;
    public float swipeAngle = 0;
    public float swipeResist = 1f;
    private Vector2 currentTouch;
    public bool hasTouched;

    public bool isBomb;
    public bool isFreeze;
    public GameObject bomb;
    public GameObject freeze;

    // Start is called before the first frame update
    void Start()
    {
        isBomb = false;
        board = FindObjectOfType<BoardManager>();
        findMatches = FindObjectOfType<FindMatches>();
        //targetX = (int)transform.position.x;
        //targetY = (int)transform.position.y;
        //column = targetX;
        //row = targetY;
        //previousColumn = column;
        //previousRow = row;
    }

    // Update is called once per frame
    void Update()
    {
        Touch();



        if (isMatched)
        {
            //gameObject.SetActive(false);
        }

        targetX = column;
        targetY = row;

        if(Mathf.Abs(targetX - transform.position.x) > 0.1f)
        {
            //Move towards target
            tempTouchPos = new Vector2(targetX, transform.position.y);
            transform.position = Vector2.Lerp(transform.position, tempTouchPos, 0.6f);
            if(board.allFruits[column,row]!= gameObject)
            {
                board.allFruits[column, row] = gameObject;
            }
            findMatches.FindAllMatches();
        }
        else
        {
            //Set the pos directly
            tempTouchPos = new Vector2(targetX, transform.position.y);
            transform.position = tempTouchPos;
            board.allFruits[column, row] = gameObject;
        }
        if (Mathf.Abs(targetY - transform.position.y) > 0.1f)
        {
            //Move towards target
            tempTouchPos = new Vector2(transform.position.x, targetY);
            transform.position = Vector2.Lerp(transform.position, tempTouchPos, 0.6f);
            if (board.allFruits[column, row] != gameObject)
            {
                board.allFruits[column, row] = gameObject;
            }
            findMatches.FindAllMatches();
        }
        else
        {
            //Set the pos directly
            tempTouchPos = new Vector2(transform.position.x, targetY);
            transform.position = tempTouchPos;
        }
    }


    public IEnumerator CheckMoveCo()
    {
        yield return new WaitForSeconds(.5f);
        if(otherFruit != null)
        {
            if (!isMatched && !otherFruit.GetComponent<Fruit>().isMatched)
            {
                otherFruit.GetComponent<Fruit>().row = row;
                otherFruit.GetComponent<Fruit>().column = column;
                row = previousRow;
                column = previousColumn;
                yield return new WaitForSeconds(.5f);
                board.currentFruit = null;
                board.currentState = GameState.move;
            }
            else
            {
                board.DestroyMatches();
            }
            //otherFruit = null;
        }
        
    }

    private void Touch()
    {

        if (board.currentState == GameState.move)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began && !hasTouched)
                {
                    Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                    RaycastHit hit;

                    if (Physics.Raycast(raycast, out hit))
                    {
                        firstTouchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);
                        hasTouched = true;
                    }
                }
                if(Input.GetTouch(i).phase == TouchPhase.Ended && hasTouched)
                {
                    finalTouchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);
                    hasTouched = false;
                    CalculateAngle();
                }
            }
        }
    }

    private void OnMouseDown()
    {
        if(board.currentState == GameState.move)
        {
            firstTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
    private void OnMouseUp()
    {
        if(board.currentState == GameState.move)
        {
            finalTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            CalculateAngle();
        }
    }

    void CalculateAngle()
    {
        if(Mathf.Abs(finalTouchPos.y - firstTouchPos.y ) > swipeResist || Mathf.Abs(finalTouchPos.x - firstTouchPos.x) > swipeResist)
        {
            swipeAngle = Mathf.Atan2(finalTouchPos.y - firstTouchPos.y, finalTouchPos.x - firstTouchPos.x) * 180/Mathf.PI;
            MoveFruit();
            board.currentState = GameState.wait;
            board.currentFruit = this;
        }
        else
        {
            board.currentState = GameState.move;
        }
    }

    void MoveFruit()
    {
        if(swipeAngle > -45 && swipeAngle <= 45 && column < board.width-1)
        {
            //Right Swipe
            otherFruit = board.allFruits[column + 1, row];
            previousColumn = column;
            previousRow = row;
            otherFruit.GetComponent<Fruit>().column -= 1;
            column += 1;
        }
        else if (swipeAngle > 45 && swipeAngle <= 135 && row < board.height-1)
        {
            //Up Swipe
            otherFruit = board.allFruits[column, row + 1];
            previousColumn = column;
            previousRow = row;
            otherFruit.GetComponent<Fruit>().row -= 1;
            row += 1;
        }
        else if ((swipeAngle > 135 || swipeAngle <= -135) && column > 0)
        {
            //Left Swipe
            otherFruit = board.allFruits[column - 1, row];
            previousColumn = column;
            previousRow = row;
            otherFruit.GetComponent<Fruit>().column += 1;
            column -= 1;
        }
        else if (swipeAngle < -45 && swipeAngle >= -135 && row > 0)
        {
            //Down Swipe
            otherFruit = board.allFruits[column, row - 1];
            previousColumn = column;
            previousRow = row;
            otherFruit.GetComponent<Fruit>().row += 1;
            row -= 1;
        }

        StartCoroutine(CheckMoveCo());
    }


    private void FindMatches()
    {
        if(column > 0 && column < board.width - 1)
        {
            GameObject leftFruit = board.allFruits[column - 1, row];
            GameObject rightFruit = board.allFruits[column + 1, row];
            if(leftFruit != null && rightFruit != null)
            {
                if(leftFruit.tag == gameObject.tag && rightFruit.tag == gameObject.tag)
                {
                    leftFruit.GetComponent<Fruit>().isMatched = true;
                    rightFruit.GetComponent<Fruit>().isMatched = true;
                    isMatched = true;
                }
            }

        }
        if (row > 0 && row < board.height - 1)
        {
            GameObject upFruit = board.allFruits[column, row + 1];
            GameObject downFruit = board.allFruits[column, row - 1];

            if (upFruit != null && downFruit != null)
            {
                if (upFruit.tag == gameObject.tag && downFruit.tag == gameObject.tag)
                {
                    upFruit.GetComponent<Fruit>().isMatched = true;
                    downFruit.GetComponent<Fruit>().isMatched = true;
                    isMatched = true;
                }
            }
            

        }
    }



    public void MakeBomb()
    {
        //QUI SI INSTANZIANO LE BOMBE! SE SI CONDIZIONANO QUESTE TRE ISTRUZIONI SUCCESSIVE TUTTO CIO' CHE DERIVA DALLA LORO PRESENZA NON ACCADE!
        isBomb = true;
        GameObject bombGO = Instantiate(bomb, transform.position, Quaternion.identity);
        bombGO.transform.parent = transform;
        transform.GetChild(0).gameObject.SetActive(false);      //Questa riga disabilita il body dei frutti, cos� rimane soltanto quello della bomba!    
    }
    public void MakeFreeze()
    {
        //QUI SI INSTANZIANO I FREEZE! SE SI CONDIZIONANO QUESTE TRE ISTRUZIONI SUCCESSIVE TUTTO CIO' CHE DERIVA DALLA LORO PRESENZA NON ACCADE!
        isFreeze = true;
        GameObject freezeGO = Instantiate(freeze, transform.position, Quaternion.identity);
        freezeGO.transform.parent = transform;
        transform.GetChild(0).gameObject.SetActive(false);      //Questa riga disabilita il body dei frutti, cos� rimane soltanto quello del freeze!    
    }


    public void FreezeTimer()
    {
        board.currentFreezeTimer = 0;
    }



}
