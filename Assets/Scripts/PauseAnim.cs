using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseAnim : MonoBehaviour
{
    public Animator anim;
    public PointManager pointmng;

    private void Start()
    {
        pointmng = FindObjectOfType<PointManager>();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        anim.SetBool("PauseFade", true);
    }

    public void Resume()
    {
        anim.SetBool("PauseFade", false);
        Time.timeScale = 1;
    }
    public void Restart()
    {
        anim.SetBool("FadeInOut", true);
        StartCoroutine(WaitingFade("Game Scene"));
    }

    public void Home()
    {
        
        anim.SetBool("FadeInOut", true);
        StartCoroutine(WaitingFade("Main Scene"));
    }

    public IEnumerator WaitingFade(string scene)
    {
        pointmng.SaveFinalScore();
        Time.timeScale = 1;
        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene(scene);
    }

    public void GameOverCall()
    {
        anim.SetBool("GameOver", true);
        Pause();
    }

}
