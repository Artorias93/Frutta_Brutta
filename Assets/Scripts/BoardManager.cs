using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public enum GameState
{
    wait,
    move,
    lose
}

public class BoardManager : MonoBehaviour
{

    public GameState currentState = GameState.move;
    public int width;
    public int height;
    public int offset;
    public GameObject tilePrefab;
    private Tile[,] allTiles;
    public GameObject[] fruits;
    public GameObject[,] allFruits;
    public FindMatches findMatches;
    public Fruit currentFruit;
    public PauseAnim pause;


    private float timer;
    public float maxTimer;
    public TextMeshProUGUI text;
    public bool isTimerFrozen = false;
    public bool isGameOver;

    [SerializeField]
    private bool hasChosenBomb;

    public bool HasChosenBomb
    {
        get
        {
            if (FindObjectOfType<GameManager>() != null)
            {
                return FindObjectOfType<GameManager>().HasChosenBomb;
            }
            else return hasChosenBomb;
        }
    }

    public float freezeTimer;
    [Range(0, 100)]
    public float bombSpawnPercent;

    [HideInInspector]
    public float currentFreezeTimer;



    private void Start()
    {
        pause = FindObjectOfType<PauseAnim>();
        findMatches = FindObjectOfType<FindMatches>();
        allTiles = new Tile[width, height];
        allFruits = new GameObject[width, height];
        Setup();
        timer = maxTimer;
        currentFreezeTimer = freezeTimer + 1;
    }

    private void Update()
    {

        if (!isTimerFrozen)
        {
            Timer();
        }

        UnfreezeTimer();
    }

    private void Setup()
    {
        for (int a = 0; a < width; a++)
        {
            for (int b = 0; b < height; b++)
            {
                Vector2 tempPos = new Vector2(a, b + offset);
                GameObject Tile = Instantiate(tilePrefab, new Vector2(a, b), Quaternion.identity);
                Tile.transform.parent = this.transform;
                Tile.name = "(" + a + ", " + b + " )";
                int fruitToUse = Random.Range(0, fruits.Length);
                int maxIterations = 0;
                while (MatchesAt(a, b, fruits[fruitToUse]) && maxIterations < 100)
                {
                    fruitToUse = Random.Range(0, fruits.Length);
                    maxIterations++;
                }
                maxIterations = 0;
                GameObject fruit = Instantiate(fruits[fruitToUse], tempPos, Quaternion.identity);
                fruit.GetComponent<Fruit>().row = b;
                fruit.GetComponent<Fruit>().column = a;
                fruit.transform.parent = transform;
                fruit.name = "Fruit (" + a + ", " + b + " )";
                allFruits[a, b] = fruit;
            }

        }
    }

    private bool MatchesAt(int column, int row, GameObject piece)
    {
        if (column > 1 && row > 1)
        {
            if (allFruits[column - 1, row].tag == piece.tag && allFruits[column - 2, row].tag == piece.tag)
            {
                return true;
            }
            if (allFruits[column, row - 1].tag == piece.tag && allFruits[column, row - 2].tag == piece.tag)
            {
                return true;
            }
        }
        else if (column <= 1 || row <= 1)
        {
            if (row > 1)
            {
                if (allFruits[column, row - 1].tag == piece.tag && allFruits[column, row - 2].tag == piece.tag)
                {
                    return true;
                }
            }
            if (column > 1)
            {
                if (allFruits[column - 1, row].tag == piece.tag && allFruits[column - 2, row].tag == piece.tag)
                {
                    return true;
                }
            }
        }
        return false;
    }


    private void DestroyMatchesAt(int column, int row)
    {
        if (allFruits[column, row].GetComponent<Fruit>().isMatched)
        {
            PointManager pointManager = FindObjectOfType<PointManager>();
            pointManager.UpdatePoints(pointManager.CalculatePoints(findMatches.currentMatches.Count));
            if (findMatches.currentMatches.Count >= 4)
            {
                findMatches.CheckPowerUp();
            }
            findMatches.currentMatches.Remove(allFruits[column, row]);
            Destroy(allFruits[column, row]);
            allFruits[column, row] = null;
            currentFruit = null;
        }
    }

    public void DestroyMatches()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allFruits[i, j] != null)
                {
                    DestroyMatchesAt(i, j);
                }
            }
        }
        StartCoroutine(DecreaseRowCo());
    }

    private IEnumerator DecreaseRowCo()
    {
        int nullCount = 0;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allFruits[i, j] == null)
                {
                    nullCount++;
                }
                else if (nullCount > 0)
                {
                    allFruits[i, j].GetComponent<Fruit>().row -= nullCount;
                    allFruits[i, j] = null;
                }
            }
            nullCount = 0;
        }

        yield return new WaitForSeconds(0.4f);
        StartCoroutine(FillBoardCo());
    }

    private void RefillBoard()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allFruits[i, j] == null)
                {
                    Vector2 tempPosition = new Vector2(i, j + offset);
                    int fruitToUse = Random.Range(0, fruits.Length);
                    GameObject piece = Instantiate(fruits[fruitToUse], tempPosition, Quaternion.identity);
                    allFruits[i, j] = piece;
                    piece.GetComponent<Fruit>().row = j;
                    piece.GetComponent<Fruit>().column = i;
                }
            }
        }
    }

    private bool MatchesOnBoard()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allFruits[i, j] != null)
                {
                    if (allFruits[i, j].GetComponent<Fruit>().isMatched)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private IEnumerator FillBoardCo()
    {
        RefillBoard();
        yield return new WaitForSeconds(.5f);

        while (MatchesOnBoard())
        {
            yield return new WaitForSeconds(.5f);
            DestroyMatches();
        }
        findMatches.currentMatches.Clear();
        currentFruit = null;
        yield return new WaitForSeconds(.5f);

        if (IsDeadLocked())
        {
            GameOver();
        }
        currentState = GameState.move;

    }



    private void Timer()
    {
        timer -= Time.deltaTime;
        text.text = ((int)timer).ToString();
        if (timer <= 0 && currentState != GameState.lose)
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        if (!isGameOver)
        {
            isGameOver = true;
            currentState = GameState.lose;
            StopAllCoroutines();
            pause.GameOverCall();
        }
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }



    private void UnfreezeTimer()
    {
        currentFreezeTimer += Time.deltaTime;
        if (currentFreezeTimer <= freezeTimer)
        {
            isTimerFrozen = true;
        }
        else isTimerFrozen = false;
    }


    public void FreezeTimer()
    {
        currentFreezeTimer = 0;
    }


    //Helper methods to check for matches on board


    private void SwitchPieces(int column, int row, Vector2 direction)
    {
        GameObject holder = allFruits[column + (int)direction.x, row + (int)direction.y] as GameObject;

        allFruits[column + (int)direction.x, row + (int)direction.y] = allFruits[column, row];

        allFruits[column, row] = holder;
    }

    private bool CheckForMatches()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (allFruits[i, j] != null)
                {
                    if (i < width - 2)
                    {
                        //Check for matches to the right
                        if (allFruits[i + 1, j] != null && allFruits[i + 2, j] != null)
                        {
                            if (allFruits[i + 1, j].tag == allFruits[i, j].tag && allFruits[i + 2, j].tag == allFruits[i, j].tag)
                            {
                                return true;
                            }
                        }
                    }
                    if (j < height - 2)
                    {
                        //Check for matches up
                        if (allFruits[i, j + 1] != null && allFruits[i, j + 2] != null)
                        {
                            if (allFruits[i, j + 1].tag == allFruits[i, j].tag && allFruits[i, j + 2].tag == allFruits[i, j].tag)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }


    private bool SwitchAndCheck(int column, int row, Vector2 direction)
    {
        SwitchPieces(column, row, direction);
        if (CheckForMatches())
        {
            SwitchPieces(column, row, direction);
            return true;
        }
        SwitchPieces(column, row, direction);
        return false;
    }


    private bool IsDeadLocked()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if(allFruits[i,j] != null)
                {
                    if(i < width - 1)
                    {
                        if(SwitchAndCheck(i, j, Vector2.right))
                        {
                            return false;
                        }
                    }
                    if(j < height - 1)
                    {
                        if (SwitchAndCheck(i, j, Vector2.up))
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
