using UnityEngine.Audio;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;


public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;

    public static AudioManager instance;

    public string[] LevelNames;
    private string currentLevelName;

    private GameObject player;
    private GameObject TriggerWind_0;
    private GameObject TriggerWind_1;

    public string currentBackgroundMusic;

    private float distanceToTriggerWind = 15;



    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    private void Start()
    {

        currentLevelName = SceneManager.GetActiveScene().name;
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        switch (currentLevelName)
        {
            case "DemoScene":
                TriggerWind_0 = GameObject.FindGameObjectWithTag("TriggerWind_0");
                TriggerWind_1 = GameObject.FindGameObjectWithTag("TriggerWind_1");
                LoadAllBackgroundMusic();
                Play("Vento");
                break;
        }
    }

    private void Update()
    {
        switch (currentLevelName)
        {
            case "DemoScene":

                PlayCurrentLevelMusic();

                CheckForTriggerWind(TriggerWind_0);

                CheckForTriggerWind(TriggerWind_1);

                break;
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Play();
    }

    private void CheckForTriggerWind(GameObject triggerWind)
    {
        float distanceToWind = Mathf.Abs(player.transform.position.x - triggerWind.transform.position.x);
        Sound vento = Array.Find(sounds, sound => sound.name == "Vento");
        Debug.Log(distanceToWind);
        if (distanceToWind <= 15)
        {
            vento.source.volume = CalculatePercentage(distanceToWind);
        }
        else
        {
            vento.source.volume = 0;
        }
    }

    private void PlayCurrentLevelMusic()
    {
        Sound level1 = Array.Find(sounds, sound => sound.name == "Level1");
        Sound level2 = Array.Find(sounds, sound => sound.name == "Level2");
        Sound currentMusic = Array.Find(sounds, sound => sound.name == currentBackgroundMusic);

        level1.source.volume = 0;
        level2.source.volume = 0;

        currentMusic.source.volume = 1;
    }

    private float CalculatePercentage(float distanceToWind)
    {
        float Percent;
        
        Percent = distanceToWind/ distanceToTriggerWind;
        return Percent;
    }

    private void LoadAllBackgroundMusic()
    {
        Play("Level1");
        Play("Level2");
    }


    //FindObjectOfType<AudioManager>().Play("Jump"); This line of code plays the sound with that name, it can be put anywhere in any line of code and will work :)
}
