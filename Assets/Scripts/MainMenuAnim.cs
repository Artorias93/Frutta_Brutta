using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MainMenuAnim : MonoBehaviour
{
    [SerializeField] public Animator anim;
    [SerializeField] public Button start;
    [SerializeField] public Button highscores;
    [SerializeField] public Button options;
    [SerializeField] public Button exit;
    [SerializeField] public TextMeshProUGUI highScoreText;
    [SerializeField] public TextMeshProUGUI highScoreText1;
    [SerializeField] public TextMeshProUGUI highScoreText2;
    [SerializeField] public TextMeshProUGUI highScoreText3;
    [SerializeField] public TextMeshProUGUI highScoreText4;
    [SerializeField] public TextMeshProUGUI highScoreText5;




    private void Start()
    {
        StartCoroutine(WaitingFade());
        SetHighScore();
    }

    public void Menu()
    {
        anim.SetBool("PowerUp", false);
        anim.SetBool("Score", false);
        anim.SetBool("Little", false);
    }

    public void PowerUp()
    {
        anim.SetBool("PowerUp", true);
        anim.SetBool("Little", true);
    }

    public void ScoreBoard()
    {
        anim.SetBool("Score", true);
        anim.SetBool("Little", true);
    }

    public void Game()
    {
        anim.SetBool("Game", true);
    }

    public void Exit()
    {
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void GameStart()
    {
        anim.SetBool("Game", true);
        StartCoroutine(GameStartCO());
    }


    private IEnumerator GameStartCO()
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("Game Scene");
    }

    public IEnumerator WaitingFade()
    {
        yield return new WaitForSeconds(1f);
        start.GetComponent<Image>().raycastTarget = true;
        highscores.GetComponent<Image>().raycastTarget = true;
        options.GetComponent<Image>().raycastTarget = true;
        exit.GetComponent<Image>().raycastTarget = true;

    }

    public void SetBombPowerUp()
    {
        FindObjectOfType<GameManager>().HasChosenBomb = true;
    }
    public void SetFreezePowerUp()
    {
        FindObjectOfType<GameManager>().HasChosenBomb = false;
    }


    private void SetHighScore()
    {
        highScoreText.text = PlayerPrefs.GetInt("Highscores_0").ToString();
        highScoreText1.text = PlayerPrefs.GetInt("Highscores_0").ToString();
        highScoreText2.text = PlayerPrefs.GetInt("Highscores_1").ToString();
        highScoreText3.text = PlayerPrefs.GetInt("Highscores_2").ToString();
        highScoreText4.text = PlayerPrefs.GetInt("Highscores_3").ToString();
        highScoreText5.text = PlayerPrefs.GetInt("Highscores_4").ToString();
    }

}
