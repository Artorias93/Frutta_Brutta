using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FindMatches : MonoBehaviour
{
    private BoardManager board;
    public List<GameObject> currentMatches = new List<GameObject>();
    private PointManager pointManager;
    
    // Start is called before the first frame update
    void Start()
    {
        board = FindObjectOfType<BoardManager>();
        pointManager = FindObjectOfType<PointManager>();
    }

    public void FindAllMatches()
    {
        StartCoroutine(FindAllMatchesCo());
    }

    private IEnumerator FindAllMatchesCo()
    {
        yield return new WaitForSeconds(.2f);
        for(int i = 0; i< board.width; i++)
        {
            for (int j = 0; j < board.height; j++)
            {
                GameObject currentFruit = board.allFruits[i, j];
                if(currentFruit != null)
                {
                    if (i > 0 && i < board.width - 1)
                    {
                        GameObject leftFruit = board.allFruits[i - 1, j];
                        GameObject rightFruit = board.allFruits[i + 1, j];

                        
                        if(leftFruit != null && rightFruit != null)
                        {
                            if(leftFruit.tag == currentFruit.tag && rightFruit.tag == currentFruit.tag)
                            {
                                if(currentFruit.GetComponent<Fruit>().isBomb || leftFruit.GetComponent<Fruit>().isBomb || rightFruit.GetComponent<Fruit>().isBomb)
                                {
                                    currentMatches.Union(GetBombRowPieces(j));
                                }
                                if (currentFruit.GetComponent<Fruit>().isFreeze || leftFruit.GetComponent<Fruit>().isFreeze || rightFruit.GetComponent<Fruit>().isFreeze)
                                {
                                    board.FreezeTimer();
                                }


                                if (!currentMatches.Contains(leftFruit))
                                {
                                    currentMatches.Add(leftFruit);
                                }
                                leftFruit.GetComponent<Fruit>().isMatched = true;
                                if (!currentMatches.Contains(rightFruit))
                                {
                                    currentMatches.Add(rightFruit);
                                }
                                rightFruit.GetComponent<Fruit>().isMatched = true;
                                if (!currentMatches.Contains(currentFruit))
                                {
                                    currentMatches.Add(currentFruit);
                                }
                                currentFruit.GetComponent<Fruit>().isMatched = true;
                            }
                        }
                    }

                    if (j > 0 && j < board.height - 1)
                    {
                        GameObject upFruit = board.allFruits[i, j + 1];
                        GameObject downFruit = board.allFruits[i, j - 1];
                        if (upFruit != null && downFruit != null)
                        {
                            if (upFruit.tag == currentFruit.tag && downFruit.tag == currentFruit.tag)
                            {
                                if (currentFruit.GetComponent<Fruit>().isBomb || upFruit.GetComponent<Fruit>().isBomb || downFruit.GetComponent<Fruit>().isBomb)
                                {
                                    currentMatches.Union(GetBombColumnPieces(i));
                                }
                                if (currentFruit.GetComponent<Fruit>().isFreeze || upFruit.GetComponent<Fruit>().isFreeze || downFruit.GetComponent<Fruit>().isFreeze)
                                {
                                    board.FreezeTimer();
                                }

                                if (!currentMatches.Contains(upFruit))
                                {
                                    currentMatches.Add(upFruit);
                                }
                                upFruit.GetComponent<Fruit>().isMatched = true;
                                if (!currentMatches.Contains(downFruit))
                                {
                                    currentMatches.Add(downFruit);
                                }
                                downFruit.GetComponent<Fruit>().isMatched = true;
                                if (!currentMatches.Contains(currentFruit))
                                {
                                    currentMatches.Add(currentFruit);
                                }
                                currentFruit.GetComponent<Fruit>().isMatched = true;
                            }
                        }
                    }
                }
            }
        }

        

        


    }


    List<GameObject> GetBombRowPieces(int row)
    {

        List < GameObject > fruits = new List<GameObject>();
        for(int i = 0; i < board.width; i++)
        {
            if(board.allFruits[i, row] != null)
            {
                fruits.Add(board.allFruits[i, row]);
                board.allFruits[i, row].GetComponent<Fruit>().isMatched = true;
            }
        }

        return fruits;
    }
    List<GameObject> GetBombColumnPieces(int column)
    {

        List < GameObject > fruits = new List<GameObject>();
        for(int i = 0; i < board.height; i++)
        {
            if(board.allFruits[column, i] != null)
            {
                fruits.Add(board.allFruits[column, i]);
                board.allFruits[column, i].GetComponent<Fruit>().isMatched = true;
            }
        }

        return fruits;
    }

    public void CheckPowerUp()
    {
        if (board.currentFruit != null)
        {
            if (board.currentFruit.isMatched)
            {
                board.currentFruit.isMatched = false;
                float spawnBomb = Random.Range(1,100);
                if(spawnBomb <= board.bombSpawnPercent)
                {
                    if (board.HasChosenBomb)
                    {
                        board.currentFruit.MakeBomb();
                    }
                    else board.currentFruit.MakeFreeze();
                }
            }
            else if (board.currentFruit.otherFruit != null)
            {
                Fruit otherFruit = board.currentFruit.otherFruit.GetComponent<Fruit>();
                if (otherFruit.isMatched)
                {
                    otherFruit.isMatched = false;

                    float typeOfBomb = Random.Range(1, 100);
                    if (typeOfBomb <= board.bombSpawnPercent)
                    {
                        if (board.HasChosenBomb) 
                        {
                            otherFruit.MakeBomb();
                        }
                        else board.currentFruit.MakeFreeze();
                        
                    }
                }
            }
        }
    }
}
