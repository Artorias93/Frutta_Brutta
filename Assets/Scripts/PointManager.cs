using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PointManager : MonoBehaviour
{
    public TextMeshProUGUI pointText;
    private FindMatches findMatches;
    List<int> highscores = new List<int> { 0, 0, 2, 0, 0 };
    [HideInInspector]
    public int currentPoints;

    public int pointsPerPiece;

    // Start is called before the first frame update
    void Start()
    {
        findMatches = FindObjectOfType<FindMatches>();
    }

    // Update is called once per frame
    void Update()
    {
        pointText.text = currentPoints.ToString();
    }

    public int CalculatePoints(int numberOfPieces)
    {
        int numberOfPoints = numberOfPieces * pointsPerPiece;
        return numberOfPoints;
    }

    public void UpdatePoints(int pointsToAdd)
    {
        currentPoints = currentPoints + pointsToAdd;
    }

    public void SaveFinalScore()
    {
        for (int i = 0; i < 5; i++) highscores[i] = PlayerPrefs.GetInt("Highscores_" + i);

        highscores.Add(currentPoints);
        highscores.Sort();
        highscores.Reverse();

        for (int i = 0; i < 5; i++) PlayerPrefs.SetInt("Highscores_" + i, highscores[i]);
    }

}
