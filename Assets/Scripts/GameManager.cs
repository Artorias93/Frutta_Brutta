using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    List<int> highscores;
    [SerializeField]
    private bool hasChosenBomb;
    public bool HasChosenBomb
    {
        get { return hasChosenBomb; }
        set { hasChosenBomb = value; }
    }

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        for (int i = 0; i < 5; i++)
            PlayerPrefs.SetInt("Highscores_" + i, 0);

        DontDestroyOnLoad(gameObject);

    }
    
}
